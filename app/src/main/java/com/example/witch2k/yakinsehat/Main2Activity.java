package com.example.witch2k.yakinsehat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {
    private ImageView vi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        vi = (ImageView) findViewById(R.id.vi);
        Animation myanime = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        vi.startAnimation(myanime);
        Thread thread = new Thread(){
            public void run(){
                try{
                    sleep(4000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally {
                    startActivity(new Intent(Main2Activity.this,MainActivity.class));
                    finish();
                }
            }
        };

            thread.start();
    }
}
