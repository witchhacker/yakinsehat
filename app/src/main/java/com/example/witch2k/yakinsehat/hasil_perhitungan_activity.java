package com.example.witch2k.yakinsehat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;

public class hasil_perhitungan_activity extends AppCompatActivity {
    double Berat;
    double Tinggi;
    double Perut;
    String gender = "";

    private TextView text_berat_ideal, Gizi, KadarLemak, kalori;
    FirebaseDatabase database_firebase;
    DatabaseReference database_reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_perhitungan_activity);

        //Get intent
        Intent intent = getIntent();
        initializeDatabase();

        //Show result in textview
        text_berat_ideal = (TextView) findViewById(R.id.berat_ideal);
        Gizi = (TextView) findViewById(R.id.status_gizi);
        KadarLemak = (TextView) findViewById(R.id.kadar_lemak);
        kalori = (TextView) findViewById(R.id.kalori);

        //Get string from main activity
        String nama = intent.getStringExtra(MainActivity.send_nama);
        String jenis_kelamin = intent.getStringExtra(MainActivity.send_jenis_kelamin);
        String tanggal_lahir = intent.getStringExtra(MainActivity.send_tgl_lahir);
        String text_umur = intent.getStringExtra(MainActivity.send_umur);
        String text_tinggi_badan = intent.getStringExtra(MainActivity.send_tinggi_badan);
        String text_berat_badan = intent.getStringExtra(MainActivity.send_berat_badan);
        String text_lingkar_perut = intent.getStringExtra(MainActivity.send_lingkar_perut);
        String tingkat_keaktifan = intent.getStringExtra(MainActivity.send_tingkat_aktivitas);

        //Parse to int
        int umur, tinggi_badan, berat_badan, lingkar_perut;
        umur = Integer.parseInt(text_umur);
        tinggi_badan = Integer.parseInt(text_tinggi_badan);
        berat_badan = Integer.parseInt(text_berat_badan);
        lingkar_perut = Integer.parseInt(text_lingkar_perut);

        //Get date of today
        Date today = Calendar.getInstance().getTime();
        String text_today = String.valueOf(today);

        //Transfer variabel local to global
        Berat = berat_badan;
        Tinggi = tinggi_badan;
        Perut = lingkar_perut;
        gender = jenis_kelamin;


        //Show in textview
        int BBideal = show_berat_ideal(jenis_kelamin, tinggi_badan);
        show_status_gizi(berat_badan, tinggi_badan);
        String Status_Gizi = Gizi.getText().toString();
        show_kadar_lemak();
        String Kadar_Lemak = KadarLemak.getText().toString();
        double kebutuhan_kalori = show_kebutuhan_kalori(tingkat_keaktifan, umur);

        //Save to database
        DataPengguna pengguna = new DataPengguna();
        pengguna.setNama(nama);
        pengguna.setJenis_kelamin(jenis_kelamin);
        pengguna.setTanggal_lahir(tanggal_lahir);
        pengguna.setUmur(text_umur);
        pengguna.setTinggi_badan(text_tinggi_badan);
        pengguna.setBerat_badan(text_berat_badan);
        pengguna.setLingkar_perut(text_lingkar_perut);
        pengguna.setTingkat_keaktifan(tingkat_keaktifan);
        pengguna.setBerat_ideal(String.valueOf(BBideal));
        pengguna.setStatus_gizi(Status_Gizi);
        pengguna.setKadar_lemak(Kadar_Lemak);
        pengguna.setKebutuhan_kalori(String.valueOf(kebutuhan_kalori));
        pengguna.setToday(text_today);

        database_reference.child("Pengguna").child(nama).setValue(pengguna);
    }

    private int show_berat_ideal(String jenis_kelamin, int tinggi_badan) {
        int BBideal = 0;
        if (jenis_kelamin.equals("Pria")) {
            BBideal = (tinggi_badan - 100) - ((tinggi_badan - 100) / 10);
            text_berat_ideal.setText("Berat Badan Ideal : \n" + String.valueOf(BBideal) + "kg");
        } else if (jenis_kelamin.equals("Wanita")) {
            BBideal = (tinggi_badan - 100) - ((tinggi_badan - 100) * 15 / 100);
            text_berat_ideal.setText("Berat Badan Ideal : \n" +String.valueOf(BBideal) + "kg");
        }
        return BBideal;
    }

    private void show_status_gizi(int berat_badan, int tinggi_badan) {
        double HitungIMT,TinggiM,berat_double;
        TinggiM = (double)tinggi_badan / 100;
        berat_double = (double)berat_badan;

        if (berat_double != 0 || TinggiM != 0)
        {
            HitungIMT = berat_double / (TinggiM * TinggiM);
        }
        else
        {
            HitungIMT = 0;
        }

        if(HitungIMT < 18.5) {
            Gizi.setText("Status Gizi :\n KURUS\n" + "IMT = " + String.valueOf(HitungIMT));
        } else if (HitungIMT >= 18.5 && HitungIMT < 25) {
            Gizi.setText("Status Gizi :\n NORMAL\n" + "IMT = " + String.valueOf(HitungIMT));
        } else if (HitungIMT >= 25 && HitungIMT < 30) {
            Gizi.setText("Status Gizi :\n KEGEMUKAN\n" + "IMT = " + String.valueOf(HitungIMT));
        } else if (HitungIMT >= 30 && HitungIMT < 35) {
            Gizi.setText("Status Gizi :\n OBESITAS TINGKAT 1\n" + "IMT = " + String.valueOf(HitungIMT));
        } else if (HitungIMT >= 35 && HitungIMT < 40) {
            Gizi.setText("Status Gizi :\n OBESITAS TINGKAT 2\n" + "IMT = " + String.valueOf(HitungIMT));
        } else if (HitungIMT >= 40) {
            Gizi.setText("Status Gizi :\n OBESITAS TINGKAT 3\n" + "IMT = " + String.valueOf(HitungIMT));
        }
    }

    private void initializeDatabase() {
        FirebaseApp.initializeApp(hasil_perhitungan_activity.this);
        database_firebase = FirebaseDatabase.getInstance();
        database_reference = database_firebase.getReference();
    }

    private double show_kebutuhan_kalori(String keaktifan, int usia) {
        double BMR = 0;
        double nilai_keaktifan = 0;
        if (keaktifan.equals("Tidak Aktif")) {
            nilai_keaktifan = 1.200;
        } else if (keaktifan.equals("Aktivitas Ringan")) {
            nilai_keaktifan = 1.375;
        } else if (keaktifan.equals("Aktivitas Sedang")) {
            nilai_keaktifan = 1.550;
        } else if (keaktifan.equals("Aktivitas Berat")) {
            nilai_keaktifan = 1.725;
        } else if (keaktifan.equals("Aktivitas Sangat Berat")) {
            nilai_keaktifan = 1.900;
        }
        if (gender.equals("Pria")) {
            BMR = 66.473 + (13.7516 * Berat) + (5.003 * Tinggi) - (6.7550 * usia);
        } else if (gender.equals("Wanita")) {
            BMR = 655.0955 + (9.5634 * Berat) + (1.8496 * Tinggi) - (4.6756 * usia);
        }
        double kebkalori = BMR * nilai_keaktifan;
        kalori.setText("Kebutuhan Kalori : \n" + String.valueOf(kebkalori) + "kkal");
        return kebkalori;
    }

    private void show_kadar_lemak() {
        if(gender.equals("Pria")) {
            //Laki-Laki
            //Ringan{
            //TinggiBadan{
            double
                    TBR1 = 181, TBR2 = 178, TBR3 = 175, TBR4 = 173, TBR5 = 173, TBR6 = 170, TBR7 = 169, TBR8 = 169, TBR9 = 158, TBR10 = 157,
                    TBR11 = 155, TBR12 = 154, TBR13 = 154, TBR14 = 152, TBR15 = 151;
            double jumTBR = TBR1 + TBR2 + TBR3 + TBR4 + TBR5 + TBR6 + TBR7 + TBR8 + TBR9 + TBR10 + TBR11 + TBR12 + TBR13 + TBR14 + TBR15;
            double MeanTBR = jumTBR / 15;
            MeanTBR = Math.floor(MeanTBR * 100) / 100;

            double SDTBR = ((15 * ((TBR1 * TBR1) + (TBR2 * TBR2) + (TBR3 * TBR3) + (TBR4 * TBR4) + (TBR5 * TBR5) + (TBR6 * TBR6) + (TBR7 * TBR7) +
                    (TBR8 * TBR8) + (TBR9 * TBR9) + (TBR10 * TBR10) + (TBR11 * TBR11) + (TBR12 * TBR12) + (TBR13 * TBR13) + (TBR14 * TBR14) +
                    (TBR15 * TBR15))) - (jumTBR * jumTBR)) / (15 * 14);
            SDTBR = Math.sqrt(SDTBR);
            SDTBR = Math.floor(SDTBR * 100) / 100;

            double PTBR = -Math.pow((Tinggi - MeanTBR), 2) / (2 * Math.pow(SDTBR, 2));
            double FTBR = (1 / (Math.sqrt(2 * 3.14) * SDTBR)) * Math.pow(2.72, PTBR);
            FTBR = Math.floor(FTBR * 100000) / 100000;


            //}
            //BeratBadan{
            double BBR1 = 58, BBR2 = 98, BBR3 = 66, BBR4 = 61, BBR5 = 82, BBR6 = 91, BBR7 = 108, BBR8 = 95, BBR9 = 99, BBR10 = 89,
                    BBR11 = 126, BBR12 = 50, BBR13 = 82, BBR14 = 73, BBR15 = 95;
            double jumBBR = BBR1 + BBR2 + BBR3 + BBR4 + BBR5 + BBR6 + BBR7 + BBR8 + BBR9 + BBR10 + BBR11 + BBR12 + BBR13 + BBR14 + BBR15;
            double MeanBBR = jumBBR / 15;
            MeanBBR = Math.floor(MeanBBR * 100) / 100;

            double SDBBR = ((15 * ((BBR1 * BBR1) + (BBR2 * BBR2) + (BBR3 * BBR3) + (BBR4 * BBR4) + (BBR5 * BBR5) + (BBR6 * BBR6) + (BBR7 * BBR7) +
                    (BBR8 * BBR8) + (BBR9 * BBR9) + (BBR10 * BBR10) + (BBR11 * BBR11) + (BBR12 * BBR12) + (BBR13 * BBR13) + (BBR14 * BBR14) +
                    (BBR15 * BBR15))) - (jumBBR * jumBBR)) / (15 * 14);
            SDBBR = Math.sqrt(SDBBR);
            SDBBR = Math.floor(SDBBR * 100) / 100;
            double PBBR = -Math.pow((Berat - MeanBBR), 2) / (2 * Math.pow(SDBBR, 2));
            double FBBR = (1 / (Math.sqrt(2 * 3.14) * SDBBR)) * Math.pow(2.72, PBBR);
            FBBR = Math.floor(FBBR * 100000) / 100000;
            //}

            //LingkarPerut{
            double LPR1 = 53, LPR2 = 67, LPR3 = 51, LPR4 = 52, LPR5 = 66, LPR6 = 50, LPR7 = 67, LPR8 = 61, LPR9 = 74, LPR10 = 50,
                    LPR11 = 74, LPR12 = 54, LPR13 = 77, LPR14 = 52, LPR15 = 59;
            double jumLPR = LPR1 + LPR2 + LPR3 + LPR4 + LPR5 + LPR6 + LPR7 + LPR8 + LPR9 + LPR10 + LPR11 + LPR12 + LPR13 + LPR14 + LPR15;
            double MeanLPR = jumLPR / 15;
            MeanLPR = Math.floor(MeanLPR * 100) / 100;
            double SDLPR = ((15 * ((LPR1 * LPR1) + (LPR2 * LPR2) + (LPR3 * LPR3) + (LPR4 * LPR4) + (LPR5 * LPR5) + (LPR6 * LPR6) + (LPR7 * LPR7) +
                    (LPR8 * LPR8) + (LPR9 * LPR9) + (LPR10 * LPR10) + (LPR11 * LPR11) + (LPR12 * LPR12) + (LPR13 * LPR13) + (LPR14 * LPR14) +
                    (LPR15 * LPR15))) - (jumLPR * jumLPR)) / (15 * 14);
            SDLPR = Math.sqrt(SDLPR);
            SDLPR = Math.floor(SDLPR * 100) / 100;

            double PLPR = -Math.pow((Perut - MeanLPR), 2) / (2 * Math.pow(SDLPR, 2));
            double FLPR = (1 / (Math.sqrt(2 * 3.14) * SDLPR)) * Math.pow(2.72, PLPR);
            //}
            //}


            //NORMAL{

            //TinggiBadan{
            double
                    TBN1 = 189, TBN2 = 189, TBN3 = 186, TBN4 = 185, TBN5 = 172, TBN6 = 172, TBN7 = 164, TBN8 = 156, TBN9 = 154;
            double jumTBN = TBN1 + TBN2 + TBN3 + TBN4 + TBN5 + TBN6 + TBN7 + TBN8 + TBN9;
            double MeanTBN = jumTBN / 9;
            MeanTBN = Math.floor(MeanTBN * 100) / 100;
            double SDTBN = ((9 * ((TBN1 * TBN1) + (TBN2 * TBN2) + (TBN3 * TBN3) + (TBN4 * TBN4) + (TBN5 * TBN5) + (TBN6 * TBN6) + (TBN7 * TBN7) +
                    (TBN8 * TBN8) + (TBN9 * TBN9))) - (jumTBN * jumTBN)) / (9 * 8);
            SDTBN = Math.sqrt(SDTBN);
            SDTBN = Math.floor(SDTBN * 100) / 100;

            double PTBN = -Math.pow((Tinggi - MeanTBN), 2) / (2 * Math.pow(SDTBN, 2));
            double FTBN = (1 / (Math.sqrt(2 * 3.14) * SDTBN)) * Math.pow(2.72, PTBN);
            FTBN = Math.floor(FTBN * 100000) / 100000;
            //}

            //BeratBadan{
            double
                    BBN1 = 103, BBN2 = 59, BBN3 = 127, BBN4 = 68, BBN5 = 91, BBN6 = 82, BBN7 = 99, BBN8 = 85, BBN9 = 108;
            double jumBBN = BBN1 + BBN2 + BBN3 + BBN4 + BBN5 + BBN6 + BBN7 + BBN8 + BBN9;
            double MeanBBN = jumBBN / 9;
            MeanBBN = Math.floor(MeanBBN * 100) / 100;
            double SDBBN = ((9 * ((BBN1 * BBN1) + (BBN2 * BBN2) + (BBN3 * BBN3) + (BBN4 * BBN4) + (BBN5 * BBN5) + (BBN6 * BBN6) + (BBN7 * BBN7) +
                    (BBN8 * BBN8) + (BBN9 * BBN9))) - (jumBBN * jumBBN)) / (9 * 8);
            SDBBN = Math.sqrt(SDBBN);
            SDBBN = Math.floor(SDBBN * 100) / 100;

            double PBBN = -Math.pow((Berat - MeanBBN), 2) / (2 * Math.pow(SDBBN, 2));
            double FBBN = (1 / (Math.sqrt(2 * 3.14) * SDBBN)) * Math.pow(2.72, PBBN);
            FBBN = Math.floor(FBBN * 100000) / 100000;
            //}

            //LingkarPerut{
            double
                    LPN1 = 87, LPN2 = 90, LPN3 = 82, LPN4 = 82, LPN5 = 94, LPN6 = 84, LPN7 = 97, LPN8 = 96, LPN9 = 83;
            double jumLPN = LPN1 + LPN2 + LPN3 + LPN4 + LPN5 + LPN6 + LPN7 + LPN8 + LPN9;
            double MeanLPN = jumLPN / 9;
            MeanLPN = Math.floor(MeanLPN * 100) / 100;
            double SDLPN = ((9 * ((LPN1 * LPN1) + (LPN2 * LPN2) + (LPN3 * LPN3) + (LPN4 * LPN4) + (LPN5 * LPN5) + (LPN6 * LPN6) + (LPN7 * LPN7) +
                    (LPN8 * LPN8) + (LPN9 * LPN9))) - (jumLPN * jumLPN)) / (9 * 8);
            SDLPN = Math.sqrt(SDLPN);
            SDLPN = Math.floor(SDLPN * 100) / 100;

            double PLPN = -Math.pow((Perut - MeanLPN), 2) / (2 * Math.pow(SDLPN, 2));
            double FLPN = (1 / (Math.sqrt(2 * 3.14) * SDLPN)) * Math.pow(2.72, PLPN);
            //}
            //}


            //TINGGI{

            //TinggiBadan{
            double TBT1 = 180, TBT2 = 178, TBT3 = 176, TBT4 = 176, TBT5 = 173, TBT6 = 170, TBT7 = 166, TBT8 = 165, TBT9 = 164,
                    TBT10 = 156, TBT11 = 155, TBT12 = 154;
            double jumTBT = TBT1 + TBT2 + TBT3 + TBT4 + TBT5 + TBT6 + TBT7 + TBT8 + TBT9 + TBT10 + TBT11 + TBT12;
            double MeanTBT = jumTBT / 12;
            MeanTBT = Math.floor(MeanTBT * 100) / 100;
            double SDTBT = ((12 * ((TBT1 * TBT1) + (TBT2 * TBT2) + (TBT3 * TBT3) + (TBT4 * TBT4) + (TBT5 * TBT5) + (TBT6 * TBT6) + (TBT7 * TBT7) +
                    (TBT8 * TBT8) + (TBT9 * TBT9) + (TBT10 * TBT10) + (TBT11 * TBT11) + (TBT12 * TBT12))) - (jumTBT * jumTBT))
                    / (12 * 11);
            SDTBT = Math.sqrt(SDTBT);
            SDTBT = Math.floor(SDTBT * 100) / 100;

            double PTBT = -Math.pow((Tinggi - MeanTBT), 2) / (2 * Math.pow(SDTBT, 2));
            double FTBT = (1 / (Math.sqrt(2 * 3.14) * SDTBT)) * Math.pow(2.72, PTBT);
            FTBT = Math.floor(FTBT * 100000) / 100000;
            //}

            //BeratBadan{
            double BBT1 = 115, BBT2 = 91, BBT3 = 59, BBT4 = 91, BBT5 = 118, BBT6 = 79, BBT7 = 56, BBT8 = 101, BBT9 = 86,
                    BBT10 = 80, BBT11 = 93, BBT12 = 124;
            double jumBBT = BBT1 + BBT2 + BBT3 + BBT4 + BBT5 + BBT6 + BBT7 + BBT8 + BBT9 + BBT10 + BBT11 + BBT12;
            double MeanBBT = jumBBT / 12;
            MeanBBT = Math.floor(MeanBBT * 100) / 100;
            double SDBBT = ((12 * ((BBT1 * BBT1) + (BBT2 * BBT2) + (BBT3 * BBT3) + (BBT4 * BBT4) + (BBT5 * BBT5) + (BBT6 * BBT6) + (BBT7 * BBT7) +
                    (BBT8 * BBT8) + (BBT9 * BBT9) + (BBT10 * BBT10) + (BBT11 * BBT11) + (BBT12 * BBT12))) - (jumBBT * jumBBT))
                    / (12 * 11);
            SDBBT = Math.sqrt(SDBBT);
            SDBBT = Math.floor(SDBBT * 100) / 100;

            double PBBT = -Math.pow((Berat - MeanBBT), 2) / (2 * Math.pow(SDBBT, 2));
            double FBBT = (1 / (Math.sqrt(2 * 3.14) * SDBBT)) * Math.pow(2.72, PBBT);
            FBBT = Math.floor(FBBT * 100000) / 100000;
            //}

            //LingkarPerut{
            double LPT1 = 111, LPT2 = 106, LPT3 = 110, LPT4 = 110, LPT5 = 116, LPT6 = 110, LPT7 = 113, LPT8 = 107, LPT9 = 100,
                    LPT10 = 108, LPT11 = 105, LPT12 = 102;
            double jumLPT = LPT1 + LPT2 + LPT3 + LPT4 + LPT5 + LPT6 + LPT7 + LPT8 + LPT9 + LPT10 + LPT11 + LPT12;
            double MeanLPT = jumLPT / 12;
            MeanLPT = Math.floor(MeanLPT * 100) / 100;
            double SDLPT = ((12 * ((LPT1 * LPT1) + (LPT2 * LPT2) + (LPT3 * LPT3) + (LPT4 * LPT4) + (LPT5 * LPT5) + (LPT6 * LPT6) + (LPT7 * LPT7) +
                    (LPT8 * LPT8) + (LPT9 * LPT9) + (LPT10 * LPT10) + (LPT11 * LPT11) + (LPT12 * LPT12))) - (jumLPT * jumLPT))
                    / (12 * 11);
            SDLPT = Math.sqrt(SDLPT);
            SDLPT = Math.floor(SDLPT * 100) / 100;

            double PLPT = -Math.pow((Perut - MeanLPT), 2) / (2 * Math.pow(SDLPT, 2));
            double FLPT = (1 / (Math.sqrt(2 * 3.14) * SDLPT)) * Math.pow(2.72, PLPT);
            //}
            //}


            //SANGATTINGGI{

            //TinggiBadan{
            double TBST1 = 185, TBST2 = 184, TBST3 = 182, TBST4 = 180, TBST5 = 177, TBST6 = 177, TBST7 = 176, TBST8 = 171, TBST9 = 163,
                    TBST10 = 161, TBST11 = 159, TBST12 = 158, TBST13 = 155, TBST14 = 152;
            double jumTBST = TBST1 + TBST2 + TBST3 + TBST4 + TBST5 + TBST6 + TBST7 + TBST8 + TBST9 + TBST10 + TBST11 + TBST12 + TBST13 + TBST14;
            double MeanTBST = jumTBST / 14;
            MeanTBST = Math.floor(MeanTBST * 100) / 100;
            double SDTBST = ((14 * ((TBST1 * TBST1) + (TBST2 * TBST2) + (TBST3 * TBST3) + (TBST4 * TBST4) + (TBST5 * TBST5) + (TBST6 * TBST6) +
                    (TBST7 * TBST7) + (TBST8 * TBST8) + (TBST9 * TBST9) + (TBST10 * TBST10) + (TBST11 * TBST11) + (TBST12 * TBST12) + (TBST13 * TBST13) +
                    (TBST14 * TBST14))) - (jumTBST * jumTBST)) / (14 * 13);
            SDTBST = Math.sqrt(SDTBST);
            SDTBST = Math.floor(SDTBST * 100) / 100;

            double PTBST = -Math.pow((Tinggi - MeanTBST), 2) / (2 * Math.pow(SDTBST, 2));
            double FTBST = (1 / (Math.sqrt(2 * 3.14) * SDTBST)) * Math.pow(2.72, PTBST);
            FTBST = Math.floor(FTBST * 100000) / 100000;
            //}

            //BeratBadan{
            double BBST1 = 60, BBST2 = 91, BBST3 = 116, BBST4 = 128, BBST5 = 76, BBST6 = 125, BBST7 = 109, BBST8 = 104, BBST9 = 73,
                    BBST10 = 80, BBST11 = 71, BBST12 = 101, BBST13 = 88, BBST14 = 99;
            double jumBBST = BBST1 + BBST2 + BBST3 + BBST4 + BBST5 + BBST6 + BBST7 + BBST8 + BBST9 + BBST10 + BBST11 + BBST12 + BBST13 + BBST14;
            double MeanBBST = jumBBST / 14;
            MeanBBST = Math.floor(MeanBBST * 100) / 100;
            double SDBBST = ((14 * ((BBST1 * BBST1) + (BBST2 * BBST2) + (BBST3 * BBST3) + (BBST4 * BBST4) + (BBST5 * BBST5) + (BBST6 * BBST6) +
                    (BBST7 * BBST7) + (BBST8 * BBST8) + (BBST9 * BBST9) + (BBST10 * BBST10) + (BBST11 * BBST11) + (BBST12 * BBST12) + (BBST13 * BBST13) +
                    (BBST14 * BBST14))) - (jumBBST * jumBBST)) / (14 * 13);
            SDBBST = Math.sqrt(SDBBST);
            SDBBST = Math.floor(SDBBST * 100) / 100;

            double PBBST = -Math.pow((Berat - MeanBBST), 2) / (2 * Math.pow(SDBBST, 2));
            double FBBST = (1 / (Math.sqrt(2 * 3.14) * SDBBST)) * Math.pow(2.72, PBBST);
            FBBST = Math.floor(FBBST * 100000) / 100000;
            //}

            //LingkarPerut
            double LPST1 = 120, LPST2 = 134, LPST3 = 128, LPST4 = 121, LPST5 = 120, LPST6 = 140, LPST7 = 125, LPST8 = 126, LPST9 = 124,
                    LPST10 = 123, LPST11 = 132, LPST12 = 135, LPST13 = 120, LPST14 = 122;
            double jumLPST = LPST1 + LPST2 + LPST3 + LPST4 + LPST5 + LPST6 + LPST7 + LPST8 + LPST9 + LPST10 + LPST11 + LPST12 + LPST13 + LPST14;
            double MeanLPST = jumLPST / 14;
            MeanLPST = Math.floor(MeanLPST * 100) / 100;
            double SDLPST = ((14 * ((LPST1 * LPST1) + (LPST2 * LPST2) + (LPST3 * LPST3) + (LPST4 * LPST4) + (LPST5 * LPST5) + (LPST6 * LPST6) +
                    (LPST7 * LPST7) + (LPST8 * LPST8) + (LPST9 * LPST9) + (LPST10 * LPST10) + (LPST11 * LPST11) + (LPST12 * LPST12) + (LPST13 * LPST13) +
                    (LPST14 * LPST14))) - (jumLPST * jumLPST)) / (14 * 13);
            SDLPST = Math.sqrt(SDLPST);
            SDLPST = Math.floor(SDLPST * 100) / 100;

            double PLPST = -Math.pow((Perut - MeanLPST), 2) / (2 * Math.pow(SDLPST, 2));
            double FLPST = (1 / (Math.sqrt(2 * 3.14) * SDLPST)) * Math.pow(2.72, PLPST);
            //}

            double LikelihoodRendah = FTBR * FBBR * FLPR * 0.3;
            double LikelihoodNormal = FTBN * FBBN * FLPN * 0.18;
            double LikelihoodTinggi = FTBT * FBBT * FLPT * 0.24;
            double LikelihoodSangatTinggi = FTBST * FBBST * FLPST * 0.28;

            double ProRendah = LikelihoodRendah / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);
            double ProNormal = LikelihoodNormal / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);
            double ProTinggi = LikelihoodTinggi / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);
            double ProSangatTinggi = LikelihoodSangatTinggi / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);

            if (ProSangatTinggi > ProTinggi && ProSangatTinggi > ProNormal && ProSangatTinggi > ProRendah) {
                KadarLemak.setText("Kadar Lemak :\n SANGAT TINGGI");
            } else if (ProTinggi > ProNormal && ProTinggi > ProRendah) {
                KadarLemak.setText("Kadar Lemak :\n TINGGI");
            } else if (ProNormal > ProRendah) {
                KadarLemak.setText("Kadar Lemak :\n NORMAL");
            } else {
                KadarLemak.setText("Kadar Lemak :\n RINGAN");
            }
        }

        //Perempuan
        else if (gender.equals("Wanita"))
        {
            //RINGAN{
            //TInggiBadan{
            double TBR1 = 143, TBR2 = 169, TBR3 = 178, TBR4 = 147, TBR5 = 180, TBR6 = 168, TBR7 = 176, TBR8 = 149, TBR9 = 162,
                    TBR10 = 171, TBR11 = 166, TBR12 = 159, TBR13 = 147, TBR14 = 158, TBR15 = 151, TBR16 = 178, TBR17 = 174,
                    TBR18 = 173, TBR19 = 161, TBR20 = 167;

            double jumTBR = TBR1 + TBR2 + TBR3 + TBR4 + TBR5 + TBR6 + TBR7 + TBR8 + TBR9 + TBR10 + TBR11 + TBR12 + TBR13 + TBR14 + TBR15
                    + TBR16 + TBR17 + TBR18 + TBR19 + TBR20;
            double MeanTBR = jumTBR / 20;
            MeanTBR = Math.floor(MeanTBR * 100) / 100;

            double SDTBR = ((20 * ((TBR1 * TBR1) + (TBR2 * TBR2) + (TBR3 * TBR3) + (TBR4 * TBR4) + (TBR5 * TBR5) + (TBR6 * TBR6) + (TBR7 * TBR7) +
                    (TBR8 * TBR8) + (TBR9 * TBR9) + (TBR10 * TBR10) + (TBR11 * TBR11) + (TBR12 * TBR12) + (TBR13 * TBR13) + (TBR14 * TBR14) +
                    (TBR15 * TBR15) + (TBR16 * TBR16) + (TBR17 * TBR17) + (TBR18 * TBR18) + (TBR19 * TBR19) + (TBR20 * TBR20))) - (jumTBR * jumTBR)) / (20 * 19);
            SDTBR = Math.sqrt(SDTBR);
            SDTBR = Math.floor(SDTBR * 100) / 100;

            double PTBR = -Math.pow((Tinggi - MeanTBR), 2) / (2 * Math.pow(SDTBR, 2));
            double FTBR = (1 / (Math.sqrt(2 * 3.14) * SDTBR)) * Math.pow(2.72, PTBR);
            FTBR = Math.floor(FTBR * 100000) / 100000;
            //}
            //BeratBadan{
            double BBR1 = 47, BBR2 = 54, BBR3 = 60, BBR4 = 43, BBR5 = 97, BBR6 = 48, BBR7 = 89, BBR8 = 78, BBR9 = 69, BBR10 = 50,
                    BBR11 = 74, BBR12 = 99, BBR13 = 56, BBR14 = 47, BBR15 = 76, BBR16 = 73, BBR17 = 88, BBR18 = 92, BBR19 = 63,
                    BBR20 = 61;

            double jumBBR = BBR1 + BBR2 + BBR3 + BBR4 + BBR5 + BBR6 + BBR7 + BBR8 + BBR9 + BBR10 + BBR11 + BBR12 + BBR13 + BBR14 + BBR15
                    + BBR16 + BBR17 + BBR18 + BBR19 + BBR20;
            double MeanBBR = jumBBR / 20;
            MeanBBR = Math.floor(MeanBBR * 100) / 100;

            double SDBBR = ((20 * ((BBR1 * BBR1) + (BBR2 * BBR2) + (BBR3 * BBR3) + (BBR4 * BBR4) + (BBR5 * BBR5) + (BBR6 * BBR6) + (BBR7 * BBR7) +
                    (BBR8 * BBR8) + (BBR9 * BBR9) + (BBR10 * BBR10) + (BBR11 * BBR11) + (BBR12 * BBR12) + (BBR13 * BBR13) + (BBR14 * BBR14) +
                    (BBR15 * BBR15) + (BBR16 * BBR16) + (BBR17 * BBR17) + (BBR18 * BBR18) + (BBR19 * BBR19) + (BBR20 * BBR20))) - (jumBBR * jumBBR)) / (20 * 19);
            SDBBR = Math.sqrt(SDBBR);
            SDBBR = Math.floor(SDBBR * 100) / 100;
            double PBBR = -Math.pow((Berat - MeanBBR), 2) / (2 * Math.pow(SDBBR, 2));
            double FBBR = (1 / (Math.sqrt(2 * 3.14) * SDBBR)) * Math.pow(2.72, PBBR);
            FBBR = Math.floor(FBBR * 100000) / 100000;
            //}
            //LingkarPErut{
            double LPR1 = 61, LPR2 = 64, LPR3 = 71, LPR4 = 55, LPR5 = 51, LPR6 = 57, LPR7 = 70, LPR8 = 58, LPR9 = 61, LPR10 = 67,
                    LPR11 = 61, LPR12 = 55, LPR13 = 68, LPR14 = 78, LPR15 = 55, LPR16 = 75, LPR17 = 62, LPR18 = 71, LPR19 = 73,
                    LPR20 = 51;

            double jumLPR = LPR1 + LPR2 + LPR3 + LPR4 + LPR5 + LPR6 + LPR7 + LPR8 + LPR9 + LPR10 + LPR11 + LPR12 + LPR13 + LPR14
                    + LPR15 + LPR16 + LPR17 + LPR18 + LPR19 + LPR20;
            double MeanLPR = jumLPR / 20;
            MeanLPR = Math.floor(MeanLPR * 100) / 100;

            double SDLPR = ((20 * ((LPR1 * LPR1) + (LPR2 * LPR2) + (LPR3 * LPR3) + (LPR4 * LPR4) + (LPR5 * LPR5) + (LPR6 * LPR6) + (LPR7 * LPR7) +
                    (LPR8 * LPR8) + (LPR9 * LPR9) + (LPR10 * LPR10) + (LPR11 * LPR11) + (LPR12 * LPR12) + (LPR13 * LPR13) + (LPR14 * LPR14) +
                    (LPR15 * LPR15) + (LPR16 * LPR16) + (LPR17 * LPR17) + (LPR18 * LPR18) + (LPR19 * LPR19) + (LPR20 * LPR20))) - (jumLPR * jumLPR)) / (20 * 19);
            SDLPR = Math.sqrt(SDLPR);
            SDLPR = Math.floor(SDLPR * 100) / 100;

            double PLPR = -Math.pow((Perut - MeanLPR), 2) / (2 * Math.pow(SDLPR, 2));
            double FLPR = (1 / (Math.sqrt(2 * 3.14) * SDLPR)) * Math.pow(2.72, PLPR);
            //}
            //}


            //NORMAL{
            //TInggiBadan{
            double TBN1 = 155, TBN2 = 153, TBN3 = 160, TBN4 = 162, TBN5 = 152, TBN6 = 153, TBN7 = 162, TBN8 = 176, TBN9 = 177,
                    TBN10 = 166;
            double jumTBN = TBN1 + TBN2 + TBN3 + TBN4 + TBN5 + TBN6 + TBN7 + TBN8 + TBN9 + TBN10;
            double MeanTBN = jumTBN / 10;
            MeanTBN = Math.floor(MeanTBN * 100) / 100;
            double SDTBN = ((10 * ((TBN1 * TBN1) + (TBN2 * TBN2) + (TBN3 * TBN3) + (TBN4 * TBN4) + (TBN5 * TBN5) + (TBN6 * TBN6) + (TBN7 * TBN7) +
                    (TBN8 * TBN8) + (TBN9 * TBN9) + (TBN10 * TBN10))) - (jumTBN * jumTBN)) / (10 * 9);
            SDTBN = Math.sqrt(SDTBN);
            SDTBN = Math.floor(SDTBN * 100) / 100;

            double PTBN = -Math.pow((Tinggi - MeanTBN), 2) / (2 * Math.pow(SDTBN, 2));
            double FTBN = (1 / (Math.sqrt(2 * 3.14) * SDTBN)) * Math.pow(2.72, PTBN);
            FTBN = Math.floor(FTBN * 100000) / 100000;

            //}
            //BeratBadan{
            double BBN1 = 91, BBN2 = 68, BBN3 = 48, BBN4 = 44, BBN5 = 82, BBN6 = 49, BBN7 = 83, BBN8 = 74, BBN9 = 100,
                    BBN10 = 90;
            double jumBBN = BBN1 + BBN2 + BBN3 + BBN4 + BBN5 + BBN6 + BBN7 + BBN8 + BBN9 + BBN10;
            double MeanBBN = jumBBN / 10;
            MeanBBN = Math.floor(MeanBBN * 100) / 100;
            double SDBBN = ((10 * ((BBN1 * BBN1) + (BBN2 * BBN2) + (BBN3 * BBN3) + (BBN4 * BBN4) + (BBN5 * BBN5) + (BBN6 * BBN6) + (BBN7 * BBN7) +
                    (BBN8 * BBN8) + (BBN9 * BBN9) + (BBN10 * BBN10))) - (jumBBN * jumBBN)) / (10 * 9);
            SDBBN = Math.sqrt(SDBBN);
            SDBBN = Math.floor(SDBBN * 100) / 100;

            double PBBN = -Math.pow((Berat - MeanBBN), 2) / (2 * Math.pow(SDBBN, 2));
            double FBBN = (1 / (Math.sqrt(2 * 3.14) * SDBBN)) * Math.pow(2.72, PBBN);
            FBBN = Math.floor(FBBN * 100000) / 100000;

            //}
            //LingkarPerut{
            double LPN1 = 97, LPN2 = 93, LPN3 = 82, LPN4 = 97, LPN5 = 89, LPN6 = 97, LPN7 = 93, LPN8 = 80, LPN9 = 82,
                    LPN10 = 99;
            double jumLPN = LPN1 + LPN2 + LPN3 + LPN4 + LPN5 + LPN6 + LPN7 + LPN8 + LPN9 + LPN10;
            double MeanLPN = jumLPN / 10;
            MeanLPN = Math.floor(MeanLPN * 100) / 100;
            double SDLPN = ((10 * ((LPN1 * LPN1) + (LPN2 * LPN2) + (LPN3 * LPN3) + (LPN4 * LPN4) + (LPN5 * LPN5) + (LPN6 * LPN6) + (LPN7 * LPN7) +
                    (LPN8 * LPN8) + (LPN9 * LPN9) + (LPN10 * LPN10))) - (jumLPN * jumLPN)) / (10 * 9);
            SDLPN = Math.sqrt(SDLPN);
            SDLPN = Math.floor(SDLPN * 100) / 100;

            double PLPN = -Math.pow((Perut - MeanLPN), 2) / (2 * Math.pow(SDLPN, 2));
            double FLPN = (1 / (Math.sqrt(2 * 3.14) * SDLPN)) * Math.pow(2.72, PLPN);
            //}

            //}

            //TINGGI{
            //TInggiBadan{
            double TBT1 = 140, TBT2 = 163, TBT3 = 152, TBT4 = 150, TBT5 = 179, TBT6 = 152, TBT7 = 147, TBT8 = 158, TBT9 = 170,
                    TBT10 = 174, TBT11 = 174, TBT12 = 179, TBT13 = 179, TBT14 = 147;
            double jumTBT = TBT1 + TBT2 + TBT3 + TBT4 + TBT5 + TBT6 + TBT7 + TBT8 + TBT9 + TBT10 + TBT11 + TBT12 + TBT13 + TBT14;
            double MeanTBT = jumTBT / 14;
            MeanTBT = Math.floor(MeanTBT * 100) / 100;
            double SDTBT = ((14 * ((TBT1 * TBT1) + (TBT2 * TBT2) + (TBT3 * TBT3) + (TBT4 * TBT4) + (TBT5 * TBT5) + (TBT6 * TBT6) + (TBT7 * TBT7) +
                    (TBT8 * TBT8) + (TBT9 * TBT9) + (TBT10 * TBT10) + (TBT11 * TBT11) + (TBT12 * TBT12) + (TBT13 * TBT13) + (TBT14 * TBT14))) - (jumTBT * jumTBT))
                    / (14 * 13);
            SDTBT = Math.sqrt(SDTBT);
            SDTBT = Math.floor(SDTBT * 100) / 100;

            double PTBT = -Math.pow((Tinggi - MeanTBT), 2) / (2 * Math.pow(SDTBT, 2));
            double FTBT = (1 / (Math.sqrt(2 * 3.14) * SDTBT)) * Math.pow(2.72, PTBT);
            FTBT = Math.floor(FTBT * 100000) / 100000;

            //}
            //BeratBadan{
            double BBT1 = 74, BBT2 = 52, BBT3 = 63, BBT4 = 50, BBT5 = 61, BBT6 = 78, BBT7 = 86, BBT8 = 40, BBT9 = 46,
                    BBT10 = 44, BBT11 = 59, BBT12 = 81, BBT13 = 69, BBT14 = 40;
            double jumBBT = BBT1 + BBT2 + BBT3 + BBT4 + BBT5 + BBT6 + BBT7 + BBT8 + BBT9 + BBT10 + BBT11 + BBT12 + BBT13 + BBT14;
            double MeanBBT = jumBBT / 14;
            MeanBBT = Math.floor(MeanBBT * 100) / 100;
            double SDBBT = ((14 * ((BBT1 * BBT1) + (BBT2 * BBT2) + (BBT3 * BBT3) + (BBT4 * BBT4) + (BBT5 * BBT5) + (BBT6 * BBT6) + (BBT7 * BBT7) +
                    (BBT8 * BBT8) + (BBT9 * BBT9) + (BBT10 * BBT10) + (BBT11 * BBT11) + (BBT12 * BBT12)
                    + (BBT13 * BBT13) + (BBT14 * BBT14))) - (jumBBT * jumBBT))
                    / (14 * 13);
            SDBBT = Math.sqrt(SDBBT);
            SDBBT = Math.floor(SDBBT * 100) / 100;

            double PBBT = -Math.pow((Berat - MeanBBT), 2) / (2 * Math.pow(SDBBT, 2));
            double FBBT = (1 / (Math.sqrt(2 * 3.14) * SDBBT)) * Math.pow(2.72, PBBT);
            FBBT = Math.floor(FBBT * 100000) / 100000;
            //}
            //LingkarPErut{
            double LPT1 = 102, LPT2 = 103, LPT3 = 115, LPT4 = 111, LPT5 = 106, LPT6 = 109, LPT7 = 108, LPT8 = 104, LPT9 = 102,
                    LPT10 = 101, LPT11 = 114, LPT12 = 101, LPT13 = 117, LPT14 = 102;
            double jumLPT = LPT1 + LPT2 + LPT3 + LPT4 + LPT5 + LPT6 + LPT7 + LPT8 + LPT9 + LPT10 + LPT11 + LPT12 + LPT13 + LPT14;
            double MeanLPT = jumLPT / 14;
            MeanLPT = Math.floor(MeanLPT * 100) / 100;
            double SDLPT = ((14 * ((LPT1 * LPT1) + (LPT2 * LPT2) + (LPT3 * LPT3) + (LPT4 * LPT4) + (LPT5 * LPT5) + (LPT6 * LPT6) + (LPT7 * LPT7) +
                    (LPT8 * LPT8) + (LPT9 * LPT9) + (LPT10 * LPT10) + (LPT11 * LPT11) + (LPT12 * LPT12)
                    + (LPT13 * LPT13) + (LPT14 * LPT14))) - (jumLPT * jumLPT)) / (14 * 13);
            SDLPT = Math.sqrt(SDLPT);
            SDLPT = Math.floor(SDLPT * 100) / 100;

            double PLPT = -Math.pow((Perut - MeanLPT), 2) / (2 * Math.pow(SDLPT, 2));
            double FLPT = (1 / (Math.sqrt(2 * 3.14) * SDLPT)) * Math.pow(2.72, PLPT);
            //}

            //}

            //SANGATTINGGI{
            //TInggiBadan{
            double TBST1 = 158, TBST2 = 140, TBST3 = 176, TBST4 = 163, TBST5 = 157, TBST6 = 143;
            double jumTBST = TBST1 + TBST2 + TBST3 + TBST4 + TBST5 + TBST6;
            double MeanTBST = jumTBST / 6;
            MeanTBST = Math.floor(MeanTBST * 100) / 100;
            double SDTBST = ((6 * ((TBST1 * TBST1) + (TBST2 * TBST2) + (TBST3 * TBST3) + (TBST4 * TBST4) + (TBST5 * TBST5)
                    + (TBST6 * TBST6))) - (jumTBST * jumTBST)) / (6 * 5);
            SDTBST = Math.sqrt(SDTBST);
            SDTBST = Math.floor(SDTBST * 100) / 100;

            double PTBST = -Math.pow((Tinggi - MeanTBST), 2) / (2 * Math.pow(SDTBST, 2));
            double FTBST = (1 / (Math.sqrt(2 * 3.14) * SDTBST)) * Math.pow(2.72, PTBST);
            FTBST = Math.floor(FTBST * 100000) / 100000;
            //}
            //BeratBadan{
            double BBST1 = 64, BBST2 = 54, BBST3 = 45, BBST4 = 75, BBST5 = 80, BBST6 = 99;
            double jumBBST = BBST1 + BBST2 + BBST3 + BBST4 + BBST5 + BBST6;
            double MeanBBST = jumBBST / 6;
            MeanBBST = Math.floor(MeanBBST * 100) / 100;
            double SDBBST = ((6 * ((BBST1 * BBST1) + (BBST2 * BBST2) + (BBST3 * BBST3) + (BBST4 * BBST4) + (BBST5 * BBST5)
                    + (BBST6 * BBST6))) - (jumBBST * jumBBST)) / (6 * 5);
            SDBBST = Math.sqrt(SDBBST);
            SDBBST = Math.floor(SDBBST * 100) / 100;

            double PBBST = -Math.pow((Berat - MeanBBST), 2) / (2 * Math.pow(SDBBST, 2));
            double FBBST = (1 / (Math.sqrt(2 * 3.14) * SDBBST)) * Math.pow(2.72, PBBST);
            FBBST = Math.floor(FBBST * 100000) / 100000;
            //}
            //LingkarPErut{
            double LPST1 = 125, LPST2 = 120, LPST3 = 125, LPST4 = 121, LPST5 = 130, LPST6 = 127;
            double jumLPST = LPST1 + LPST2 + LPST3 + LPST4 + LPST5 + LPST6;
            double MeanLPST = (LPST1 + LPST2 + LPST3 + LPST4 + LPST5 + LPST6) / 6;
            MeanLPST = Math.floor(MeanLPST * 100) / 100;
            double SDLPST = ((6 * ((LPST1 * LPST1) + (LPST2 * LPST2) + (LPST3 * LPST3) + (LPST4 * LPST4) + (LPST5 * LPST5)
                    + (LPST6 * LPST6))) - (jumLPST * jumLPST)) / (6 * 5);
            SDLPST = Math.sqrt(SDLPST);
            SDLPST = Math.floor(SDLPST * 100) / 100;

            double PLPST = -Math.pow((Perut - MeanLPST), 2) / (2 * Math.pow(SDLPST, 2));
            double FLPST = (1 / (Math.sqrt(2 * 3.14) * SDLPST)) * Math.pow(2.72, PLPST);
            //}

            //}
            double LikelihoodRendah = FTBR * FBBR * FLPR * 0.4;
            double LikelihoodNormal = FTBN * FBBN * FLPN * 0.2;
            double LikelihoodTinggi = FTBT * FBBT * FLPT * 0.28;
            double LikelihoodSangatTinggi = FTBST * FBBST * FLPST * 0.12;

            double ProRendah = LikelihoodRendah / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);
            double ProNormal = LikelihoodNormal / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);
            double ProTinggi = LikelihoodTinggi / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);
            double ProSangatTinggi = LikelihoodSangatTinggi / (LikelihoodRendah + LikelihoodNormal + LikelihoodTinggi + LikelihoodSangatTinggi);

            //            KadarLemak.setText(""+ProRendah);
            if (ProSangatTinggi > ProTinggi && ProSangatTinggi > ProNormal && ProSangatTinggi > ProRendah) {
                KadarLemak.setText("Kadar Lemak :\n SANGAT TINGGI");
            } else if (ProTinggi > ProNormal && ProTinggi > ProRendah) {
                KadarLemak.setText("Kadar Lemak :\n TINGGI");
            } else if (ProNormal > ProRendah) {
                KadarLemak.setText("Kadar Lemak :\n NORMAL");
            } else {
                KadarLemak.setText("Kadar Lemak :\n RINGAN");
            }
        }
    }
}
