package com.example.witch2k.yakinsehat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class histori_activity extends AppCompatActivity
{
    private final static String TAG = histori_activity.class.getSimpleName();
    private ListView mListView;

    FirebaseDatabase database_firebase;
    DatabaseReference database_reference;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori_activity);

        //The ListView
        mListView = (ListView)findViewById(R.id.listview);

        //Get intent
        Intent intent = getIntent();
        hasil_perhitungan_activity hpa = new hasil_perhitungan_activity();

        //Read data from database
        initializeDatabase();
        readData();
    }

    private void readData()
    {
        database_reference.child("Pengguna").addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                DataPengguna dp = new DataPengguna();
                ArrayList<String> array = new ArrayList<>();
                for(DataSnapshot childSnapshot : dataSnapshot.getChildren())
                {
                    dp.setNama(childSnapshot.getValue(DataPengguna.class).getNama());
                    dp.setJenis_kelamin(childSnapshot.getValue(DataPengguna.class).getJenis_kelamin());
                    dp.setTanggal_lahir(childSnapshot.getValue(DataPengguna.class).getTanggal_lahir());
                    dp.setUmur(childSnapshot.getValue(DataPengguna.class).getUmur());
                    dp.setTinggi_badan(childSnapshot.getValue(DataPengguna.class).getTinggi_badan());
                    dp.setBerat_badan(childSnapshot.getValue(DataPengguna.class).getBerat_badan());
                    dp.setLingkar_perut(childSnapshot.getValue(DataPengguna.class).getLingkar_perut());
                    dp.setTingkat_keaktifan(childSnapshot.getValue(DataPengguna.class).getTingkat_keaktifan());
                    dp.setBerat_ideal(childSnapshot.getValue(DataPengguna.class).getBerat_ideal());
                    dp.setStatus_gizi(childSnapshot.getValue(DataPengguna.class).getStatus_gizi());
                    dp.setKadar_lemak(childSnapshot.getValue(DataPengguna.class).getKadar_lemak());
                    dp.setKebutuhan_kalori(childSnapshot.getValue(DataPengguna.class).getKebutuhan_kalori());
                    dp.setToday(childSnapshot.getValue(DataPengguna.class).getToday());

                    array.add
                    (
                            "Nama : " + dp.getNama() + "\n" + dp.getToday() + "\n\n" +
                            "Jenis Kelamin : " + dp.getJenis_kelamin() + "\n" +
                            "Tanggal Lahir : " + dp.getTanggal_lahir() + "\n" +
                            "Umur : " + dp.getUmur() + "\n" +
                            "Tinggi Badan : " + dp.getTinggi_badan() + "\n" +
                            "Berat Badan : " + dp.getBerat_badan() + "\n" +
                            "Lingkar Perut : " + dp.getLingkar_perut() + "\n" +
                            "Tingkat Keaktifan : " + dp.getTingkat_keaktifan() + "\n" +
                            "Berat Ideal : " + dp.getBerat_ideal() + "\n" +
                            dp.getStatus_gizi() + "\n" +
                            dp.getKadar_lemak() + "\n" +
                            "Kebutuhan Kalori : " + dp.getKebutuhan_kalori()
                    );
                }
                ArrayAdapter adapter = new ArrayAdapter(histori_activity.this,android.R.layout.simple_list_item_1,array);
                mListView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    private void initializeDatabase()
    {
        FirebaseApp.initializeApp(histori_activity.this);
        database_firebase = FirebaseDatabase.getInstance();
        database_reference = database_firebase.getReference();
    }
}
