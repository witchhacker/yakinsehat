package com.example.witch2k.yakinsehat;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    public static final String send_nama = "com.example.witch2k.yakinsehat.extra.MESSAGE1";
    public static final String send_jenis_kelamin = "com.example.witch2k.yakinsehat.extra.MESSAGE2";
    public static final String send_tgl_lahir = "com.example.witch2k.yakinsehat.extra.MESSAGE3";
    public static final String send_tinggi_badan = "com.example.witch2k.yakinsehat.extra.MESSAGE4";
    public static final String send_berat_badan = "com.example.witch2k.yakinsehat.extra.MESSAGE5";
    public static final String send_lingkar_perut = "com.example.witch2k.yakinsehat.extra.MESSAGE6";
    public static final String send_tingkat_aktivitas = "com.example.witch2k.yakinsehat.extra.MESSAGE7";
    public static final String send_umur = "com.example.witch2k.yakinsehat.extra.MESSAGE8";

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private String tingkat_keaktifan_user;
    private String nama,jenis_kelamin,tgl_lahir,text_tinggibadan,text_beratbadan,text_lingkarperut;
    private int tinggi_badan,berat_badan,umur,lingkar_perut;

    private EditText edit_nama,edit_tanggal_lahir,edit_tinggibadan,edit_beratbadan,edit_lingkarperut;
    private RadioButton pilih_jenis_kelamin;
    private Button button_perhitungan;
    private Spinner tingkat_keaktifan_spinner;
    private TextView ket;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Define view and database
        edit_nama = (EditText)findViewById(R.id.edittext_nama);
        edit_tanggal_lahir = (EditText)findViewById(R.id.edittext_tanggal_lahir);
        edit_tinggibadan = (EditText)findViewById(R.id.edittext_tinggibadan);
        edit_beratbadan = (EditText)findViewById(R.id.edittext_beratbadan);
        edit_lingkarperut = (EditText)findViewById(R.id.edittext_lingkarperut);
        tingkat_keaktifan_spinner = (Spinner)findViewById(R.id.tingkat_keaktifan);
        final RadioGroup rg = (RadioGroup)findViewById(R.id.jenis_kelamin_radio);
        ket = (TextView)findViewById(R.id.ket_keaktifan);
        button_perhitungan = (Button)findViewById(R.id.button_hitung);

        //Set Array to tingkat_keaktifan_spinner
        if(tingkat_keaktifan_spinner != null)
        {
            tingkat_keaktifan_spinner.setOnItemSelectedListener(this);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.tingkat_keaktifan,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if(tingkat_keaktifan_spinner != null)
        {
            tingkat_keaktifan_spinner.setAdapter(adapter);
        }
    }

    public void launchPerhitunganActivity(View view)
    {
        final RadioGroup rg = (RadioGroup)findViewById(R.id.jenis_kelamin_radio);
        Log.d(LOG_TAG,"Button Clicked");

        try
        {
            //Get String and Integer from EditText
            nama = edit_nama.getText().toString();
            text_tinggibadan = edit_tinggibadan.getText().toString();
            text_beratbadan = edit_beratbadan.getText().toString();
            text_lingkarperut = edit_lingkarperut.getText().toString();
            if(text_tinggibadan.isEmpty())
            {
                tinggi_badan = 0;
            }
            else
            {
                tinggi_badan = Integer.parseInt(text_tinggibadan);
            }
            if(text_beratbadan.isEmpty())
            {
                berat_badan = 0;
            }
            else
            {
                berat_badan = Integer.parseInt(text_beratbadan);
            }
            if(text_lingkarperut.isEmpty())
            {
                lingkar_perut = 0;
            }
            else
            {
                lingkar_perut = Integer.parseInt(text_lingkarperut);
            }
            //Get age from birthdate and convert to age
            tgl_lahir = edit_tanggal_lahir.getText().toString();
            Date birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(tgl_lahir);
            Date today = Calendar.getInstance().getTime();
            umur = today.getYear() - birthdate.getYear();
            if (today.getMonth() < birthdate.getMonth()) {
                umur = umur - 1;
            } else if (today.getMonth() == birthdate.getMonth()) {
                if (today.getDate() < birthdate.getDate()) {
                    umur = umur - 1;
                }
            }
            //Get String from selected radio button
            int selected_jenis_kelamin = rg.getCheckedRadioButtonId();
            if(selected_jenis_kelamin != 1)
            {
                pilih_jenis_kelamin = (RadioButton)findViewById(selected_jenis_kelamin);
                jenis_kelamin = pilih_jenis_kelamin.getText().toString();
            }

            //Launch hasil perhitungan activity
            if(nama.isEmpty())
            {
                nama = "No Name";
            }
            Intent intent = new Intent(this,hasil_perhitungan_activity.class);
            intent.putExtra(send_nama,nama);
            intent.putExtra(send_jenis_kelamin,jenis_kelamin);
            intent.putExtra(send_tgl_lahir,tgl_lahir);
            intent.putExtra(send_tinggi_badan,String.valueOf(tinggi_badan));
            intent.putExtra(send_berat_badan,String.valueOf(berat_badan));
            intent.putExtra(send_lingkar_perut,String.valueOf(lingkar_perut));
            intent.putExtra(send_tingkat_aktivitas,tingkat_keaktifan_user);
            intent.putExtra(send_umur,String.valueOf(umur));
            startActivity(intent);
        }
        catch(Exception e)
        {
            AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
            alert.setTitle("Error");
            alert.setMessage("Tanggal Lahir tidak sesuai format");
            alert.setButton
                    (
                            AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialogInterface,int i)
                                {
                                    dialogInterface.dismiss();
                                }
                            }
                    );
            alert.show();
            umur = 0;
        }
    }

    public void launchHistoriActivity(View view)
    {
        Intent intent = new Intent(this,histori_activity.class);
        startActivity(intent);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        String keaktifan = adapterView.getItemAtPosition(i).toString();
        tingkat_keaktifan_user = keaktifan;
        if(keaktifan.equals("Tidak Aktif"))
        {
            ket.setText("Tidak berolahraga sama sekali dalam semingggu");
        }
        else if(keaktifan.equals("Aktivitas Ringan"))
        {
            ket.setText("Berolahraga sekitar 1-3 kali dalam seminggu");
        }
        else if(keaktifan.equals("Aktivitas Sedang"))
        {
            ket.setText("Berolahraga sekitar 3-5 kali dalam seminggu");
        }
        else if(keaktifan.equals("Aktivitas Berat"))
        {
            ket.setText("Berolahraga sekitar 5-6 kali dalam seminggu");
        }
        else if(keaktifan.equals("Aktivitas Sangat Berat"))
        {
            ket.setText("Berolahraga sekitar 2 kali dalam sehari, termasuk latihan fisik\n" +
                        "ekstra berat, atau memang pekerjaannya selalu beraktivitas\n" +
                        "fisik");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {
        ket.setText("");
    }
}
