package com.example.witch2k.yakinsehat;

public class DataPengguna
{
    private String nama;
    private String jenis_kelamin;
    private String tanggal_lahir;
    private String umur;
    private String tinggi_badan;
    private String berat_badan;
    private String lingkar_perut;
    private String tingkat_keaktifan;
    private String berat_ideal;
    private String kadar_lemak;
    private String kebutuhan_kalori;
    private String status_gizi;
    private String today;

    public DataPengguna()
    {

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getTinggi_badan() {
        return tinggi_badan;
    }

    public void setTinggi_badan(String tinggi_badan) {
        this.tinggi_badan = tinggi_badan;
    }

    public String getBerat_badan() {
        return berat_badan;
    }

    public void setBerat_badan(String berat_badan) {
        this.berat_badan = berat_badan;
    }

    public String getLingkar_perut() {
        return lingkar_perut;
    }

    public void setLingkar_perut(String lingkar_perut) {
        this.lingkar_perut = lingkar_perut;
    }

    public String getTingkat_keaktifan() {
        return tingkat_keaktifan;
    }

    public void setTingkat_keaktifan(String tingkat_keaktifan) {
        this.tingkat_keaktifan = tingkat_keaktifan;
    }

    public String getBerat_ideal() {
        return berat_ideal;
    }

    public void setBerat_ideal(String berat_ideal) {
        this.berat_ideal = berat_ideal;
    }

    public String getKadar_lemak() {
        return kadar_lemak;
    }

    public void setKadar_lemak(String kadar_lemak) {
        this.kadar_lemak = kadar_lemak;
    }

    public String getKebutuhan_kalori() {
        return kebutuhan_kalori;
    }

    public void setKebutuhan_kalori(String kebutuhan_kalori) {
        this.kebutuhan_kalori = kebutuhan_kalori;
    }

    public String getStatus_gizi() {
        return status_gizi;
    }

    public void setStatus_gizi(String status_gizi) {
        this.status_gizi = status_gizi;
    }

    public String getToday(){
        return today;
    }

    public void setToday(String today)
    {
        this.today = today;
    }
}
